# 修改 ~/.bashrc 添加 proxychains4配置
echo 'alias pc="proxychains4"' >> ~/.bashrc
echo 'export hostIP=`grep -oP  \"(\d+\.)+(\d+)\" /etc/resolv.conf`' >> ~/.bashrc
echo 'sudo sed -i "115c http $hostIP 10809" /etc/proxychains4.conf' >> ~/.bashrc
source ~/.bashrc
# 测试代理
proxychains4 curl myip.ipip.net
sleep 3s


# 安装nvm
proxychains4 curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

source ~/.bashrc
# 安装node-lts版本
# 可能会不成功 手动输入nvm install --lts进行安装
nvm install --lts
echo
echo "Node版本:`node --version`"
sleep 3s
# npm换淘宝源
npm config set registry https://registry.npmmirror.com/
echo
echo "npm源：`npm config get registry`"
sleep 3s

# 配置Git环境
git config --global user.name "rust8"
git config --global user.email "dch888@88.com"
