#!/bin/bash
# 备份原有源
cp -rf /etc/apt/sources.list /etc/apt/sources.list.bak
# 删除sources.list中原有内容
sed -i '1,$d' /etc/apt/sources.list
# 更换北外（bfsu)源
echo "deb https://mirrors.bfsu.edu.cn/debian/ bullseye main contrib non-free" > /etc/apt/sources.list
echo "deb https://mirrors.bfsu.edu.cn/debian/ bullseye-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb https://mirrors.bfsu.edu.cn/debian/ bullseye-backports main contrib non-free" >> /etc/apt/sources.list
echo "deb https://mirrors.bfsu.edu.cn/debian-security bullseye-security main contrib non-free" >> /etc/apt/sources.list

# 更新软件
apt update && apt -y upgrade
# 安装所需软件
apt -y install git \
curl \
neovim \
proxychains4

su  rust8